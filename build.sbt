name := "eksploracja_danych"

version := "1.0"

scalaVersion := "2.11.8"

resolvers += "Spark Packages Repo" at "http://dl.bintray.com/spark-packages/maven"

libraryDependencies ++= Seq(
  "org.apache.spark" % "spark-core_2.11" % "2.0.0",
  "org.apache.spark" % "spark-graphx_2.11" % "2.0.0",
  "databricks" % "spark-corenlp" % "0.2.0-s_2.11",
  "edu.stanford.nlp" % "stanford-corenlp" % "3.6.0" classifier "models",
  "it.uniroma1.dis.wsngroup.gexf4j" % "gexf4j" % "1.0.0"
)
