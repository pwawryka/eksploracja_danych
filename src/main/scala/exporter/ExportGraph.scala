package exporter

import org.apache.spark.graphx

case class ExportGraph(
                        nodes: Seq[(graphx.VertexId, String)],
                        edges: Seq[graphx.Edge[Double]],
                        start: Int,
                        end: Int
                      )