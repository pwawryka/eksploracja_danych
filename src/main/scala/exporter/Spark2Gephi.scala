package exporter

import java.util.UUID

import it.uniroma1.dis.wsngroup.gexf4j.core._
import it.uniroma1.dis.wsngroup.gexf4j.core.dynamic._
import it.uniroma1.dis.wsngroup.gexf4j.core.impl._
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.viz._

import scala.annotation.tailrec
import scala.collection.JavaConverters._
import scala.collection.mutable

object Spark2Gephi {
  def applyGraph(graph: Graph, graphsList: GraphsList, name: String, color: ColorImpl): Unit = {
    graphsList.foreach { exportGraph =>
      val nodeMap = (for (node <- exportGraph.nodes) yield {
        val id = node._1
        val label = node._2
        val start = exportGraph.start
        val end = exportGraph.end
        val size = exportGraph.edges.count(e => e.srcId == id || e.dstId == id)
        val gephiNode = graph
          .createNode(s"$id-$name-$start-$end")
          .setLabel(label)
          .setColor(color)
          .setSize(size)
        gephiNode.getSpells.add(new SpellImpl().setStartValue(start).setEndValue(end))
        id -> gephiNode
      }).toMap

      val edgeMap = mutable.Map[String, Edge]()

      for {
        edge <- exportGraph.edges
        if nodeMap.contains(edge.srcId) && nodeMap.contains(edge.dstId)
      } yield {
        val edgeNodes = List(edge.srcId, edge.dstId)
        val (edgeSrc, edgeDst) = (edgeNodes.min, edgeNodes.max)
        val srcNode = nodeMap(edgeSrc)
        val dstNode = nodeMap(edgeDst)
        val edgeLabel = s"${name}_${srcNode.getId}_${dstNode.getId}"
        edgeMap.get(edgeLabel) match {
          case Some(gephiEdge) => gephiEdge
            .setWeight(edge.attr.toFloat + gephiEdge.getWeight)
          case None =>
            val gephiEdge = srcNode
              .connectTo(edgeLabel, dstNode)
              .setWeight(edge.attr.toFloat)
            edgeMap(edgeLabel) = gephiEdge
            gephiEdge
        }
      }
    }
  }

  def mergeNodes(graph: Graph, first: Node, second: Node): Unit = {
    val label = first.getLabel
    val size = List(first.getSize, second.getSize).max
    val firstEdges = graph.getAllEdges.asScala
      .filter(e => e.getSource == first || e.getTarget == first)
      .map(e => (e, if (e.getSource == first) e.getTarget else e.getSource))
    val secondEdges = graph.getAllEdges.asScala
      .filter(e => e.getSource == second || e.getTarget == second)
      .map(e => (e, if (e.getSource == second) e.getTarget else e.getSource))
    val (firstStart, firstEnd) = (first.getSpells.get(0).getStartValue.asInstanceOf[Int],
      first.getSpells.get(0).getEndValue.asInstanceOf[Int])
    val (secondStart, secondEnd) = (second.getSpells.get(0).getStartValue.asInstanceOf[Int],
      second.getSpells.get(0).getEndValue.asInstanceOf[Int])
    first.getSpells.clear()
    first.getSpells.add(new SpellImpl().setStartValue(-1).setEndValue(0))
    second.getSpells.clear()
    second.getSpells.add(new SpellImpl().setStartValue(-1).setEndValue(0))
    firstEdges.foreach(_._1.getSpells.clear())
    firstEdges.foreach(_._1.getSpells.clear())
    (firstStart, firstEnd) match {
      case (start, end) if start < secondStart && end > secondEnd =>
        // second in first
        val firstBefore = graph.createNode(s"m1-computed-${first.getId}")
          .setColor(new ColorImpl(0, 0, 255))
          .setLabel(label).setSize(first.getSize)
        firstBefore.getSpells.add(new SpellImpl().setStartValue(start).setEndValue(secondStart))
        val firstAfter = graph.createNode(s"m2-computed-${first.getId}")
          .setColor(new ColorImpl(0, 0, 255))
          .setLabel(label).setSize(first.getSize)
        firstAfter.getSpells.add(new SpellImpl().setStartValue(secondEnd).setEndValue(end))
        val common = graph.createNode(s"common-${UUID.randomUUID().toString}")
          .setColor(new ColorImpl(255, 0, 255))
          .setLabel(label).setSize(size)
        common.getSpells.add(new SpellImpl().setStartValue(secondStart).setEndValue(secondEnd))
        firstEdges.foreach { case (edge, dst) =>
          firstBefore.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(start).setEndValue(secondStart))
          firstAfter.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(secondEnd).setEndValue(end))
          common.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(secondStart).setEndValue(secondEnd))
        }
        secondEdges.foreach { case (edge, dst) =>
          common.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(secondStart).setEndValue(secondEnd))
        }
      case (start, end) if start > secondStart && end < secondEnd =>
        // first in second
        val secondBefore = graph.createNode(UUID.randomUUID().toString)
          .setColor(new ColorImpl(255, 0, 0))
          .setLabel(label).setSize(second.getSize)
        secondBefore.getSpells.add(new SpellImpl().setStartValue(secondStart).setEndValue(start))
        val secondAfter = graph.createNode(UUID.randomUUID().toString)
          .setColor(new ColorImpl(255, 0, 0))
          .setLabel(label).setSize(second.getSize)
        secondAfter.getSpells.add(new SpellImpl().setStartValue(end).setEndValue(secondEnd))
        val common = graph.createNode(UUID.randomUUID().toString)
          .setColor(new ColorImpl(255, 0, 255))
          .setLabel(label).setSize(size)
        common.getSpells.add(new SpellImpl().setStartValue(start).setEndValue(end))
        firstEdges.foreach { case (edge, dst) =>
          secondBefore.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(secondStart).setEndValue(start))
          secondAfter.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(end).setEndValue(secondEnd))
          common.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(start).setEndValue(end))
        }
        secondEdges.foreach { case (edge, dst) =>
          common.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(start).setEndValue(end))
        }
      case (start, end) if start < secondStart =>
        // first // second
        val firstBefore = graph.createNode(UUID.randomUUID().toString)
          .setColor(new ColorImpl(0, 0, 255))
          .setLabel(label).setSize(first.getSize)
        firstBefore.getSpells.add(new SpellImpl().setStartValue(start).setEndValue(secondStart))
        val secondAfter = graph.createNode(UUID.randomUUID().toString)
          .setColor(new ColorImpl(255, 0, 0))
          .setLabel(label).setSize(second.getSize)
        secondAfter.getSpells.add(new SpellImpl().setStartValue(end).setEndValue(secondEnd))
        val common = graph.createNode(UUID.randomUUID().toString)
          .setColor(new ColorImpl(255, 0, 255))
          .setLabel(label).setSize(size)
        common.getSpells.add(new SpellImpl().setStartValue(secondStart).setEndValue(end))
        firstEdges.foreach { case (edge, dst) =>
          firstBefore.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(start).setEndValue(secondStart))
          common.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(secondStart).setEndValue(end))
        }
        secondEdges.foreach { case (edge, dst) =>
          secondAfter.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(end).setEndValue(secondEnd))
          common.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(secondStart).setEndValue(end))
        }
      case (start, end) if start > secondStart =>
        // second // first
        val secondBefore = graph.createNode(UUID.randomUUID().toString)
          .setColor(new ColorImpl(255, 0, 0))
          .setLabel(label).setSize(second.getSize)
        secondBefore.getSpells.add(new SpellImpl().setStartValue(secondStart).setEndValue(start))
        val firstAfter = graph.createNode(UUID.randomUUID().toString)
          .setColor(new ColorImpl(0, 0, 255))
          .setLabel(label).setSize(first.getSize)
        firstAfter.getSpells.add(new SpellImpl().setStartValue(secondEnd).setEndValue(end))
        val common = graph.createNode(UUID.randomUUID().toString)
          .setColor(new ColorImpl(255, 0, 255))
          .setLabel(label).setSize(size)
        common.getSpells.add(new SpellImpl().setStartValue(start).setEndValue(secondEnd))
        firstEdges.foreach { case (edge, dst) =>
          firstAfter.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(secondEnd).setEndValue(end))
          common.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(start).setEndValue(secondEnd))
        }
        secondEdges.foreach { case (edge, dst) =>
          secondBefore.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(secondStart).setEndValue(start))
          common.connectTo(UUID.randomUUID().toString, dst)
            .getSpells.add(new SpellImpl().setStartValue(start).setEndValue(secondEnd))
        }
    }
  }

  @tailrec
  def searchIntersection(graph: Graph, label: String): Unit = {
    val nodeList = graph.getNodes.asScala.toList.filter(_.getLabel == label)
    val intersections = nodeList.flatMap { node =>
      val spell = node.getSpells.asScala.head
      val start = spell.getStartValue.asInstanceOf[Int]
      val end = spell.getEndValue.asInstanceOf[Int]
      if (start > 0) {
        val idSpec = if (node.getId.contains("computed")) "computed" else "original"
        val others = nodeList.filterNot(_ == node).filterNot(other => other.getId.contains(idSpec))
        val intersection = others.filter { other =>
          val otherSpell = other.getSpells.asScala.head
          val otherStart = otherSpell.getStartValue.asInstanceOf[Int]
          val otherEnd = otherSpell.getEndValue.asInstanceOf[Int]
          otherStart > start && otherStart < end || otherEnd > start && otherEnd < end
        }
        if (intersection.isEmpty)
          List.empty
        else
          List((node, intersection.head))
      } else {
        List.empty
      }
    }

    intersections match {
      case Nil =>
      case head :: _ =>
        println(s"intersection ${head._1.getId} ${head._2.getId}")
        mergeNodes(graph, head._1, head._2)
        searchIntersection(graph, label)
    }
  }

  def merge(graph: Graph): Unit = {
    graph.getNodes.asScala.map(_.getLabel).toSet[String].foreach { label =>
      searchIntersection(graph, label)
    }
  }

  def apply(gexf: Gexf, computedGraph: GraphsList, originalGraph: GraphsList): Graph = {
    val graph = gexf.getGraph
    gexf.getGraph
      .setDefaultEdgeType(EdgeType.UNDIRECTED)
      .setMode(Mode.DYNAMIC)
      .setTimeType(TimeFormat.INTEGER)

    applyGraph(graph, computedGraph, "computed", new ColorImpl(0, 0, 255))
//    applyGraph(graph, originalGraph, "original", new ColorImpl(255, 0, 0))

    val maxSize = graph.getNodes.asScala.map(_.getSize).max
    graph.getNodes.asScala.foreach { node =>
      node.setSize(50 + 50 * node.getSize / maxSize)
    }

    val maxWeight = graph.getAllEdges.asScala.map(_.getWeight).max
    graph.getAllEdges.asScala.foreach { edge =>
      edge.setThickness(10 + 10 * edge.getWeight / maxWeight)
    }

    merge(graph)
    graph
  }
}
