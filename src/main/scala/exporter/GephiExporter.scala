package exporter

import java.io.{File, FileWriter}
import java.util.Calendar

import it.uniroma1.dis.wsngroup.gexf4j.core.impl._

class GephiExporter(computedGraph: GraphsList, originalGraph: GraphsList) {
  val gexf = {
    val gexf = new GexfImpl
    gexf.getMetadata
      .setLastModified(Calendar.getInstance().getTime)
      .setCreator("Palka, Wawryka")
      .setDescription("Eksploracja danych 2016")
    gexf.setVisualization(true)
    gexf
  }

  val graph = Spark2Gephi(gexf, computedGraph, originalGraph)

  def export(file: File): Unit = {
    val writer = new StaxGraphWriter
    val fileWriter = new FileWriter(file, false)
    writer.writeToStream(gexf, fileWriter, "UTF-8")
  }
}

object GephiExporter {
  def apply(computedGraph: GraphsList, originalGraph: GraphsList): GephiExporter =
    new GephiExporter(computedGraph, originalGraph)
}