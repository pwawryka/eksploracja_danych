import scala.util.Random

object Generator {
  val nodesNum = 100
  val edgesNum = 500
  val path = "data/alice_adventures_in_wonderland"
  val rand = Random

  def loadWords(): List[String] = {
    scala.io.Source.fromFile(path).getLines
      .flatMap(line => line.toLowerCase.split(" ")).toSet
      .filter(word => "\\w+".r.pattern.matcher(word).matches())
      .toList
  }

  def generateEdges(nodes: List[(Long, String)]): List[(Long, Long, Int)] = {
    (for (_ <- 1 to edgesNum) yield {(
      nodes(rand.nextInt(nodesNum / 2))._1,
      nodes(rand.nextInt(nodesNum / 2) + nodesNum / 2 - 1)._1
    )}).distinct.map(e => (e._1, e._2, rand.nextInt(10) + 1)).toList
  }

  def getGraphs(n: Int): List[(List[(Long, String)] , List[(Long, Long, Int)])] = {
    val nodes = loadWords()
      .slice(0, nodesNum)
      .zipWithIndex.map(e => (e._2.toLong, e._1))
    (for (_ <- 1 to n) yield {
      (nodes, generateEdges(nodes))
    }).toList
  }
}
