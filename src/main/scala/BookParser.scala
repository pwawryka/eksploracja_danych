import java.io.InputStream

import scala.io.Source

trait BookParser {
  def originalChapters: Seq[String]
  def chapterlessText: String
}

class GutenbergParser(input: InputStream) extends BookParser {
  val originalChapters: Seq[String] = {
    val linesIter = Source.fromInputStream(input).getLines()

    Iterator.continually{
      val documentLines = linesIter.takeWhile(!_.startsWith("CHAPTER"))
      documentLines.mkString(" ")
    }.takeWhile(_ => linesIter.nonEmpty)
      .filter(_.nonEmpty)
      .toSeq
  }

  val chapterlessText: String = originalChapters.mkString(" ")
}
