import org.apache.spark.graphx.{Graph, _}

trait ClusterMeasuresCalculator {
  def calculateClusterMeasures(graph: Graph[String, Double], keyWordsCount: Int): ClusterMeasures
}

object ClusterMeasuresCalculator{
  def valueOrdering[A] = new math.Ordering[(A, Double)] {
    def compare(x: (A, Double), y: (A, Double)): Int = {
      if(x._2 > y._2) 1 else if(x._2 < y._2) -1 else 0
    }
  }
}

import ClusterMeasuresCalculator._

case class PageRankCalculator(significantWordsCount: Int) extends ClusterMeasuresCalculator {
  def calculateClusterMeasures(graph: Graph[String, Double], keyWordsCount: Int): ClusterMeasures = {
    val topWordsWithValues = graph
      .staticPageRank(5)
      .vertices
      .top(keyWordsCount max significantWordsCount)(valueOrdering)

    ClusterMeasures(
      keyWords = topWordsWithValues.take(keyWordsCount).map(_._1),
      coeff = topWordsWithValues.take(significantWordsCount).map(_._2).sum
    )
  }
}

case class TopDegreeSumCalculator(significantWordsCount: Int) extends ClusterMeasuresCalculator {
  def calculateClusterMeasures(graph: Graph[String, Double], keyWordsCount: Int): ClusterMeasures = {
    val topWordsWithValues = graph
      .collectEdges(EdgeDirection.In)
      .mapValues{ edges =>
        edges.map(_.attr).sum
      }.top(keyWordsCount max significantWordsCount)(valueOrdering)

    ClusterMeasures(
      keyWords = topWordsWithValues.take(keyWordsCount).map(_._1),
      coeff = topWordsWithValues.take(significantWordsCount).map(_._2).sum
    )
  }
}
