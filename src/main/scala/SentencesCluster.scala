import org.apache.spark.graphx.{Graph, VertexId}

case class SentencesCluster(
                             firstSentenceIdx: Int,
                             lastSentenceIdx: Int,
                             graph: Graph[String, Double],
                             measures: ClusterMeasures
                           ) {
  lazy val coeff = measures.coeff

  def sentencesIndices = firstSentenceIdx to lastSentenceIdx
}

case class ClusterMeasures(
  keyWords: Seq[Long],
  coeff: Double
                          ) {
  //def topValuesSum: Double
}

/*
case class PageRankMeasures(
  topRanks: Seq[(Long, Double)]
                           ) extends ClusterMeasures {

  def keyWords = topRanks.map(_._1)
  def coeff =
}*/
