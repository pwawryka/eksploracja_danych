import scala.util.matching.Regex

object CommandLineHandler {
  val params = """(\w+) (\d+) (\d+) (\d+) (\d+) (.+)""".r

  object ClusterMeasuresCalculatorParams {
    implicit class RegexContext(sc: StringContext) {
      def r = new Regex(sc.parts.mkString, sc.parts.tail.map(_ => "x"): _*)
    }

    def unapply(s: String): Option[ClusterMeasuresCalculator] = s match {
      case r"""pagerank (\d+)${significantWordsCount}""" => Some(new PageRankCalculator(significantWordsCount.toInt))
      case r"""degree (\d+)${significantWordsCount}""" => Some(new TopDegreeSumCalculator(significantWordsCount.toInt))
      case _ => None
    }
  }

  def handleLine(line: String) = {
    line match {
      case params(
        outputName,
        associationRadius,
        clusterInitialSentencesCount,
        keyWordsCount,
        maxClusteringIterationsCount,
        ClusterMeasuresCalculatorParams(calculator)
      ) => {
        Main.computeAndSaveResults(
          outputName,
          associationRadius.toInt,
          clusterInitialSentencesCount.toInt,
          keyWordsCount.toInt,
          maxClusteringIterationsCount.toInt,
          calculator
        )
      }

      case "exit" => {}

      case _ => println(
        """
          |Usage:
          |outputName associationRadius clusterInitialSentencesCount keyWordsCount maxClusteringIterationsCount degree significantWordsCount
          |outputName associationRadius clusterInitialSentencesCount keyWordsCount maxClusteringIterationsCount pagerank significantWordsCount
          |outputName associationRadius clusterInitialSentencesCount keyWordsCount maxClusteringIterationsCount spath significantWordsCount
        """.stripMargin)
    }
  }
}
