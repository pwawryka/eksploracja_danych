import org.apache.spark.graphx.{Graph, VertexRDD}

class ChaptersDivisor(nlpService: NlpService,
                      associationService: AssociationService,
                      calculator: ClusterMeasuresCalculator,
                      associationRadius: Int,
                      associationStrength: Int => Int => Double,
                      keyWordsCount: Int
                     ) {

  val sc = nlpService.sc

  val allNodes = sc.parallelize(
    nlpService.lemmasForIds.toSeq
  ).cache()

  /*private def mergeClusters(
                             cluster1: SentencesCluster,
                             cluster2: SentencesCluster,
                             mergedClustersCache: Map[(Int, Int), SentencesCluster]
                           ): SentencesCluster = {
    val firstSentenceIdx = cluster1.firstSentenceIdx
    val lastSentenceIdx = cluster2.lastSentenceIdx

    mergedClustersCache.get(firstSentenceIdx, lastSentenceIdx).getOrElse{
      val mergedGraph = mergeSubgraphs(Seq(cluster1.graph, cluster2.graph)).persist()

      val measures = calculator.calculateClusterMeasures(mergedGraph, keyWordsCount)

      //mergedGraph.edges.unpersist()

      anyClustersMerged = true

      val mergedCluster = SentencesCluster(
        firstSentenceIdx,
        lastSentenceIdx,
        graph = mergedGraph,
        measures
      )

      val tuple = (firstSentenceIdx, lastSentenceIdx) -> mergedCluster
      mergedClusters = mergedClusters + tuple

      mergedCluster
    }
  }*/


  private def getCluster(
                          firstSentenceIdx: Int,
                          lastSentenceIdx: Int,
                          clustersCache: Map[(Int, Int), SentencesCluster]
                        ): (SentencesCluster, Boolean) = {
    clustersCache.get(firstSentenceIdx, lastSentenceIdx) match {
      case Some(cluster) =>
        (cluster, false)

      case None => {
        val words = nlpService.getSentencesOfLemmaIds(firstSentenceIdx, lastSentenceIdx).flatten

        val vertices = words.distinct.map(lemmaId => lemmaId -> nlpService.lemmasForIds(lemmaId))

        val edges = associationService.wordsAssociations(words, associationRadius, associationStrength)

        val graph = Graph[String, Double](sc.parallelize(vertices), sc.parallelize(edges)).cache()

        val measures = calculator.calculateClusterMeasures(graph, keyWordsCount)

        val cluster = SentencesCluster(
          firstSentenceIdx,
          lastSentenceIdx,
          graph,
          measures
        )

        (cluster, true)
      }
    }
  }


  def getChaptersDivision(calculator: ClusterMeasuresCalculator, clusterInitialSentencesCount: Int, keyWordsCount: Int, maxIterationsCount: Int): Seq[SentencesCluster] = {
    var clustersCache = Map.empty[(Int, Int), SentencesCluster]

    var bottom = (0 until nlpService.sentencesCount by clusterInitialSentencesCount).map { clusterStartSentenceIdx =>
      val clusterEndSentenceIdx = clusterStartSentenceIdx + clusterInitialSentencesCount

      val (cluster, isNewCluster) = getCluster(clusterStartSentenceIdx, clusterEndSentenceIdx, clustersCache)
      if(isNewCluster){
        clustersCache += (clusterStartSentenceIdx, clusterEndSentenceIdx) -> cluster
      }

      cluster
    }

    var anyClustersMerged = true

    var iteration = 0

    while(iteration < maxIterationsCount && anyClustersMerged){
      iteration += 1

      System.err.println(s"iteration $iteration")

      anyClustersMerged = false

      val middle = bottom.sliding(2).toStream.map { case Seq(cluster1, cluster2) =>
        val (cluster, isNewCluster) = getCluster(cluster1.firstSentenceIdx, cluster2.lastSentenceIdx, clustersCache)
        if(isNewCluster){
          clustersCache += (cluster1.firstSentenceIdx, cluster2.lastSentenceIdx) -> cluster
          anyClustersMerged = true
        }

        cluster
      }

      val middleValuesPairs = (Seq(0.0) ++ middle.map(_.coeff) ++ Seq(0.0)).sliding(2).toList
      val decisions = bottom.zip(middleValuesPairs).map{
        case (bottomCluster, Seq(left, right)) =>
          val center = bottomCluster.coeff
          /*val decision = if(center > left && center > right){
            0
          } else {
            if(left >= right) -1 else 1
          }*/
          val ratherLeft = left - center
          val ratherRight = right - center
          (bottomCluster, (ratherLeft, ratherRight))
      }

      def chooseMerges(decisions: List[(SentencesCluster, (Double, Double))]): List[SentencesCluster] = {
        decisions match {
          case Nil => Nil

          case (leftCluster, (leftToLeft, leftToRight)) :: (rightCluster, (rightToLeft, rightToRight)) :: tail
            if (leftToLeft < leftToRight
              && rightToLeft > rightToRight
              && leftToRight + rightToLeft > 0) =>
            val merged = getCluster(leftCluster.firstSentenceIdx, rightCluster.lastSentenceIdx, clustersCache)._1
            merged :: chooseMerges(tail)

          case (cluster, _) :: tail =>
            cluster :: chooseMerges(tail)
        }
      }

      bottom = chooseMerges(decisions.to).toIndexedSeq
    }

    bottom
  }
}
