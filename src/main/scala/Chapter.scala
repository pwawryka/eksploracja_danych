import org.apache.spark.graphx.{Graph, VertexId}

case class Chapter(
                    text: String,
                    graph: Graph[String, Double],
                    firstSentenceIdx: Int,
                    lastSentenceIdx: Int,
                    keyWords: Seq[(VertexId, String)],
                    title: String
                  )
