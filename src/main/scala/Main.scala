import org.apache.spark.{SparkConf, SparkContext}
import java.io.{File, PrintWriter}
import java.nio.file.Paths

import exporter.{ExportGraph, GephiExporter}
import org.apache.spark.graphx.{Edge, VertexId}

import scala.io.StdIn._

object Main extends App {

  val inputName = args(0)

  val bookPath = s"/${inputName}.txt"
  val bookStream = getClass.getResourceAsStream(bookPath)

  val bookParser = inputName match {
    case "alice" | "dartagnan" => new GutenbergParser(bookStream)
  }

  val stoplistStream = getClass.getResourceAsStream("/stoplist.txt")


  val config = new SparkConf()
    .setMaster("local[*]")
    .setAppName("TextSna")
    .set("spark.executor.memory", "512m")

  val sc = new SparkContext(config)

  sc.setLogLevel("ERROR")

  val nlpService = new NlpService(sc, bookParser, stoplistStream)


  val prompt = ">> "
  var line = ""

  do {
    line = readLine(prompt)
    CommandLineHandler.handleLine(line)
  } while (line != """exit""")



  def writeAll(file: File)(texts: String*): Unit = {
    val writer = new PrintWriter(file)
    texts.foreach(writer.write)
    writer.close()
  }

  def computeAndSaveResults(
                        outputName: String,
                        associationRadius: Int,
                        clusterInitialSentencesCount: Int,
                        keyWordsCount: Int,
                        maxClusteringIterationsCount: Int,
                        clusterMeasuresCalculator: ClusterMeasuresCalculator
                      ) = {
    val resultsDir = Paths.get("results", outputName).toFile
    resultsDir.mkdirs()
    val chapteredBookFile = Paths.get("results", outputName, "result_book.txt").toFile
    val paramsFile = Paths.get("results", outputName, "params.txt").toFile
    val gephiFile = Paths.get("results", outputName, "chapters.gexf").toFile

    val associationStrength = {r: Int => d: Int => (r - d) / r.toDouble}

    val snaService = new SnaService(nlpService, associationRadius, associationStrength)

    val computedChapters = snaService.computeChapters(
      clusterMeasuresCalculator,
      clusterInitialSentencesCount,
      keyWordsCount,
      maxClusteringIterationsCount
    )

    val chaptersCoincidence = snaService.sentenceIndicesCoincidence(computedChapters)

    val computedChaptersGephiData = computedChapters.map { chapter =>
      val graph = chapter.graph.removeSelfEdges()
      val vertices = chapter.keyWords
      val edges = graph.edges.collect().toSeq
      ExportGraph(vertices, edges, chapter.firstSentenceIdx, chapter.lastSentenceIdx)
    }.toList

    val originalChaptersSentenceIdx = nlpService.sentencesInOriginalChapters
      .zipWithIndex
      .map { data =>
        val start = nlpService.sentencesInOriginalChapters.slice(0, data._2).map(_.length).sum
        (start, start + data._1.length)
      }

    val originalChaptersGephiData = snaService
      .processedOriginalChapters(keyWordsCount).toList
      .zip(originalChaptersSentenceIdx)
      .map(data => ExportGraph(data._1._1, data._1._2, data._2._1, data._2._2))

    writeAll(paramsFile)(
      s"book: $bookPath\n",
      s"associationRadius: $associationRadius\n",
      s"cluster initial sentences count: $clusterInitialSentencesCount\n",
      s"key words count: $keyWordsCount\n",
      s"max clustering iterations count: $maxClusteringIterationsCount\n",
      s"cluster measures calculator: $clusterMeasuresCalculator\n",
      "\n",
      s"chapters coincidence: $chaptersCoincidence\n"
    )

    writeAll(chapteredBookFile)(
      computedChapters.map(chapter => s"${chapter.title}\n${chapter.text}\n\n"): _*
    )

    val graphExporter = GephiExporter(computedChaptersGephiData, originalChaptersGephiData)
    graphExporter.export(gephiFile)
  }


  sc.stop()
}
