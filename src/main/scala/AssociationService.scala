import org.apache.spark.graphx.{Edge, _}

class AssociationService {
  def wordsAssociations(words: Seq[VertexId], associationRadius: Int, associationStrength: Int => Int => Double): Seq[Edge[Double]] = {
    words.indices.flatMap{ centralIdx =>
      val centralWord = words(centralIdx)
      val wordsBefore = words.slice(centralIdx - associationRadius, centralIdx)
      val wordsAfter = words.slice(centralIdx + 1, centralIdx + 1 + associationRadius)

      def associationsWithStrength(associatedWords: Seq[VertexId]) = {
        associatedWords.zipWithIndex.map{ case (word, idx) =>
          Edge(centralWord, word, associationStrength(associationRadius)(idx + 1))
        }
      }

      val associationsBefore = associationsWithStrength(wordsBefore.reverse)
      val associationsAfter = associationsWithStrength(wordsAfter)

      associationsBefore ++ associationsAfter
    }
  }
}
