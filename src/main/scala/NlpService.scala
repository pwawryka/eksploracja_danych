import java.io.InputStream

import edu.stanford.nlp.simple.{Document, Sentence}
import org.apache.spark.SparkContext
import org.apache.spark.graphx.{Graph, VertexId}

import scala.collection.JavaConverters._
import scala.io.Source

class NlpService(val sc: SparkContext, bookParser: BookParser, stoplistStream: InputStream) {

  val sentencesInOriginalChapters = bookParser.originalChapters.map{ chapterSentences =>
    new Document(chapterSentences).sentences.asScala.toSeq
  }

  val allSentencesCount = sentencesInOriginalChapters.flatten.length

  val allLemmatizedSentencesInChapters: Seq[Seq[Seq[String]]] = {
    val stopWords = Source
      .fromInputStream(stoplistStream)
      .getLines()
      .toSet

    val lowerCaseSentencesOfLemmasInChapters = sentencesInOriginalChapters.map(_.map{ sentence =>
      sentence.lemmas()
        .asScala.toSeq
        .map(_.toLowerCase)
    })

    val wordsCounts = lowerCaseSentencesOfLemmasInChapters.flatten.flatten.groupBy(identity).mapValues(_.length)

    val stopWordThreshold = allSentencesCount / 100.0

    def isMeaningful(word: String) = {
      val wordCount = wordsCounts(word)
      word.matches(".*[a-zA-Z].*") && !stopWords.contains(word) && wordCount > 1 && wordCount < stopWordThreshold
    }

    lowerCaseSentencesOfLemmasInChapters.map(_.map(_.filter(isMeaningful)))
  }

  val allLemmatizedSentences = allLemmatizedSentencesInChapters.flatten

  val parallelizedIndexedSentencesOfLemmas = sc.parallelize(allLemmatizedSentences).zipWithIndex.map(_.swap).cache()

  val idsForLemmas: Map[String, VertexId] = allLemmatizedSentences
    .flatten
    .distinct
    .zipWithIndex
    .toMap
    .mapValues(_.toLong)

  val lemmasForIds = idsForLemmas.map(_.swap)

  val sentencesOfLemmaIds = allLemmatizedSentences.map{ sentenceOfLemmas =>
    sentenceOfLemmas.map(idsForLemmas)
  }

  def getSentencesOfLemmaIds(startIdx: Int, endIdx: Int) = {
    sentencesOfLemmaIds.drop(startIdx).take(endIdx - startIdx + 1)
  }

  val lengthsOfOriginalChapters = sentencesInOriginalChapters.map(_.length)
  val indicesOfOriginalChapters = lengthsOfOriginalChapters
    .scanLeft(0)(_ + _).init
    .zip(lengthsOfOriginalChapters)
    .map{ case (start, length) => (start, start + length - 1) }

  val sentencesCount = lengthsOfOriginalChapters.sum

  val allOriginalSentences: Seq[Sentence] = {
    sentencesInOriginalChapters.flatten
  }
}
