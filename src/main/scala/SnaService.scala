import org.apache.spark.graphx.{Edge, _}

class SnaService(nlpService: NlpService, associationRadius: Int, associationStrength: Int => Int => Double) {
  val sc = nlpService.sc
  val associationService = new AssociationService

  def computeChapters(
                calculator: ClusterMeasuresCalculator,
                clusterInitialSentencesCount: Int,
                keyWordsCount: Int,
                maxClusteringIterationsCount: Int
              ): Seq[Chapter] = {
    val chaptersDivisor = new ChaptersDivisor(nlpService, associationService, calculator, associationRadius, associationStrength, keyWordsCount)

    val clusters = chaptersDivisor.getChaptersDivision(calculator, clusterInitialSentencesCount, keyWordsCount, maxClusteringIterationsCount)

    clusters.zipWithIndex.map{ case (sentencesCluster, clusterIdx) =>
      val startIdx = sentencesCluster.firstSentenceIdx
      val length = sentencesCluster.lastSentenceIdx - startIdx + 1
      val sentences = nlpService.allOriginalSentences.drop(startIdx).take(length)
      val keyWords = sentencesCluster.measures.keyWords.map(id => (id, nlpService.lemmasForIds(id)))
      Chapter(
        text = sentences.mkString(" "),
        graph = sentencesCluster.graph,
        sentencesCluster.firstSentenceIdx,
        sentencesCluster.lastSentenceIdx,
        keyWords,
        title = s"Chapter ${clusterIdx + 1}: ${keyWords.map(_._2).mkString(" ")}"
      )
    }
  }

  def sentenceIndicesCoincidence(computedChapters: Seq[Chapter]) = {
    val originalIndices = nlpService.indicesOfOriginalChapters
    val computedIndices = computedChapters.map(chapter => (chapter.firstSentenceIdx, chapter.lastSentenceIdx))

    val (shorter, longer) = {
      if(originalIndices.length < computedIndices.length)
        (originalIndices, computedIndices)
      else
        (computedIndices, originalIndices)
    }

    val coincidencesCount = shorter.map{ case (start1, end1) =>
      longer.map{ case (start2, end2) =>
        ((end1 min end2) - (start1 max start2) + 1) max 0
      }.max
    }.sum

    coincidencesCount.toDouble / nlpService.sentencesCount
  }

  def processedOriginalChapters(
                                 keyWordsCount: Int
                               ): Seq[(Seq[(VertexId, String)], Seq[Edge[Double]])] = {

    val originalClustersGraphs: Seq[Graph[String, Double]] = {
      nlpService.allLemmatizedSentencesInChapters.map{ sentencesInCluster =>

        val words = sentencesInCluster.flatten.distinct
        val wordIds = words.map(nlpService.idsForLemmas)
        val vertices = sentencesInCluster.flatten.distinct.map(lemma => nlpService.idsForLemmas(lemma) -> lemma)
        val edges = associationService.wordsAssociations(wordIds, associationRadius, associationStrength)

        Graph[String, Double](sc.parallelize(vertices), sc.parallelize(edges)).cache()
      }
    }

    originalClustersGraphs.map{ graph =>
      val topWordsWithValues = {
        graph
        .removeSelfEdges()
        .collectEdges(EdgeDirection.In)
        .mapValues{ edges =>
          edges.map(_.attr).sum
        }.top(keyWordsCount)(ClusterMeasuresCalculator.valueOrdering)
      }

      val vertices = topWordsWithValues.map{
        case (lemmaId, _) => lemmaId -> nlpService.lemmasForIds(lemmaId)
      }.toSeq
      val edges = graph.edges.collect().toSeq

      (vertices, edges)
    }
  }
}
