ZAWARTOŚĆ PŁYTKI:
	* Kod zródłowy programu - katalog program/
	* Wybrane utwory literackie (D'Artagnan, Alice in Wonderland) - katalog program/src/main/resources/
	* Dokumenty (dokumentacja, prezentacja, notatka) - katalog dokumenty/


WYMAGANIA SYSTEMOWE:
	* Java 8
	* Scala
	* Scala SBT


URUCHOMIENIE APLIKACJI:
	W celu uruchomienia aplikacji należy wykonać poniższą komendę w katalogu ze źródłami (program/)

	sbt "run {nazwa_utworu_literackiego}"

	gdzie nazwa_utworu_literackiego to nazwa pliku (z pominięciem rozszerzenia .txt), który znajduje się w katalogu program/src/main/resources/

	Po wykonaniu komendy aplikacja zostanie uruchomiona, w momencie wyświetlenia znaku zachęty (">> ") 
	należy wpisać parametry uruchomienia program lub dowolną komendę (Wtedy wyświetli się podpowiedź).
	
	Format danych wejściowych:
	outputName associationRadius clusterInitialSentencesCount keyWordsCount maxClusteringIterationsCount [degree|pagerank] significantWordsCount
	
	outputName 			- nazwa katalogu z wynikami (zostanie utworzony w momencie wykonywania programu)
	associationRadius		- promień wyznaczania asocjacji do sąsiednich wyrazów (siła asocjacji spada liniowo wraz z promieniem od 1 do 0)
	clusterInitialSentencesCount	- początkowa ilość zdań w każdym protoklastrze
	keyWordsCount			- ilość słów kluczowych (brana pod uwagę przy wizualizacji)
	maxClusteringIterationsCount	- maksymalna liczba iteracji algorytmu łączenia klastrów
	[degree|pagerank]		- wybór algorytmu obliczania jakości klastra
	significantWordsCount		- ilość słów znaczących (brana pod uwagę przy obliczaniu współczynnika jakości klastra)

	Przykładowe dane wejściowe:
	alice 7 25 6 4 degree 6

WYNIKI:
	wynik działania programu zostaje zapisany w katalogu program/results/{outputName}
	
	Pliki:
	chapters.gexf 		- graf w formacie .gexf obsługiwany przez program Gephi
	params.txt		- użyte parametry oraz końcowe pokrycie z oryginalnym podziałem
	result_book.txt		- wynikowy podział wraz ze słowami kluczowymi dla danego rozdziału
